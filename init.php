<?php

require 'src/router.php';


$router = new Router();

/***************
 * usage : 
 * addRoute(nomRegle, regle, controller, action)
 * ajouter les règles ci-dessous :
 */

//Exemple de route sans argument
//$router->addRoute("hello", "helloWorld/hello", "HelloWorld", "hello");

//Exemple de route avec argument
//$router->addRoute("hello", "helloWorld/hello/{name}", "HelloWorld", "hello");

/****************/
$router->addRoute("section", "section/{id}", "Section", "section");
$router->addRoute("sujet", "sujet/{id}", "Sujet", "sujet");
$router->addRoute("repondre", "repondre/{id}", "Formulaire", "repondre");
$router->addRoute("repondu", "repondu/{id}", "Formulaire", "newmessage");
$router->addRoute("creersujet", "creersujet/{id}", "Formulaire", "sujet");
$router->addRoute("nouveausujet", "nouveausujet/{id}", "Formulaire", "nouveausujet");
$router->addRoute("connect", "connect", "User", "connect");
$router->addRoute("inscription", "inscription", "Formulaire", "inscription");
$router->addRoute("disconnect", "disconnect", "User", "disconnect");
$router->addRoute("inscrire", "inscrire", "Formulaire", "inscrire");
//~ $router->addRoute("section", "helloWorld/hello/{name}", "HelloWorld", "hello");


//Définition de l'erreur 404
$router->set404(function() {
    echo "Erreur 404 : <br /> La ressource que vous demandez n'existe pas";
});

//Traitement des règles et lancement de la bonne fonction :
$router->run();
?>
