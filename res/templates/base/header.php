<html>
    <head>
        <title><?php echo $this->includeVar("title", "Forum"); ?></title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="<?php echo dirname($_SERVER['PHP_SELF']) ?>/res/css/styles.css" >
        <link rel ="icon" type="image/png" href = "<?php echo dirname($_SERVER['PHP_SELF']) ?>/res/img/icon_page.png" >
    </head>
    <body>
		<header>
        <div id="headercontent">
                    <a href="<?php echo dirname($_SERVER['PHP_SELF']); ?>">
			                <img src="<?php echo dirname($_SERVER['PHP_SELF']) ?>/res/img/logo_forum.jpg" alt="logo du forum"/>
                            <h1>Titre du forum</h1>
                    </a>
                <div id="login">
                <?php if (!isset($User)) { ?>
                                <form method="POST" action="<?php echo dirname($_SERVER['PHP_SELF']).'/connect'; ?>">
						<p>
						    <label>login :</label>
						    <input type="text" name="login"/>
						</p>
						<p>
						    <label>password :</label>
						    <input type="password" name="pwd"/>
						</p>
						<p class="boutonlog">
							<input type="submit" value="Sign In" />
							<a href="<?php echo dirname($_SERVER['PHP_SELF']).'/inscription'; ?>">Sign up</a>
						</p>
                    </form>
                <?php } else { ?>
                    <p>Bonjour <?php echo $User->getPseudo(); ?> !</p>
                    <a href="<?php echo dirname($_SERVER['PHP_SELF']).'/disconnect'; ?>">Se déconnecter</a>
                <?php } ?>
				</div>
		    </div>
        </header>
        <div id="test">
			<section id="content">
                <?php if (isset($infos)) {
                        foreach($infos as $info) { ?>
                            <p class="info"><?php echo $info; ?></p>
                        <?php }} ?>
