<?php $msg=$this->includeVar("msg", null); ?>
<article class="message">
	<div class="info">
		<p class="auteur"><?php  echo $msg["auteur"]; ?></p>
		<p class="date"><?php echo $msg["date"]; ?></p>
	</div>
	<p>
		<?php echo $msg["corps"]; ?>
	</p>
</article>
