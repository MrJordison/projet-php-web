<?php $this->defineVar("header", "title", $datasuj["titre"]." - Forum");
$this->includeView("header"); ?>
<a class="retour" href="<?php echo dirname($_SERVER['PHP_SELF']).'/section/'.$datasuj["idSection"]; ?>"><p class="retour">⇦ Retour au forum</p></a>
<h2><?php echo $datasuj["titre"]; ?></h2>
<?php $this->defineVar("boutonrepondre", "action", $datasuj["id"]);
$this->includeView("boutonrepondre"); ?>
<div class="listemessages">
	<?php foreach ($datasuj["messages"] as $msg) {
		$this->defineVar("message", "msg", $msg);
		$this->includeView("message");
	 }?>
</div>
<?php $this->includeView("boutonrepondre"); 
$this->includeView("footer"); ?>
