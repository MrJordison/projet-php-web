<?php $this->defineVar("header", "title", "Répondre - Forum");
$this->includeView("header"); ?>
<h2>Répondre au sujet : <?php echo $datasuj["titre"]; ?></h2>
<div class="listemessages derniersmessages" id="divmsg">
		<?php foreach ($datasuj["messages"] as $msg) {
			$this->defineVar("message", "msg", $msg);
			$this->includeView("message");
		 }?>
</div>
<form class="repondre" method="POST" action="<?php echo dirname($_SERVER['PHP_SELF']).'/repondu/'.$datasuj["id"]; ?>">
	<label>Contenu du message :</label>
	<textarea name="message" autofocus required></textarea>
	<input type="submit" value="Envoyer" />
</form>
<?php $this->includeView("footer"); ?>
<script>
	var objDiv = document.getElementById("divmsg");
	objDiv.scrollTop = objDiv.scrollHeight;
</script>
