<?php $this->defineVar("header", "title", "Nouveau sujet - Forum");
$this->includeView("header"); ?>
<h2>Créer un nouveau sujet</h2>
<form class="repondre" method="POST" action="<?php echo dirname($_SERVER['PHP_SELF']).'/nouveausujet/'.$id; ?>">
	<p>
		<label>Titre du sujet :</label>
		<input class="titrensujet" type="text" name="titre" required maxlength="50"/>
	</p>
	<label>Contenu du message :</label>
	<textarea name="message" required></textarea>
	<input type="submit" value="Envoyer" />
</form>
<?php $this->includeView("footer"); ?>
