<?php $this->defineVar("header", "title", "Inscription - Forum");
$this->includeView("header"); ?>
<h2>Inscription</h2>
<form class="repondre" method="POST" action="<?php echo dirname($_SERVER['PHP_SELF']).'/inscrire'; ?>">
	<p>
		<label>Login : </label>
		<input type="text" name="login" maxlength="20" required />
	</p>
	<p>
		<label>Email : </label>
		<input type="email" name="mail" maxlength="40" required />
	</p>
	<p>
		<label>Description : </label>
		<textarea class="description" name="description" ></textarea>
	</p>
	<p>
		<label>Mot de passe : </label>
		<input type="password" name="password" pattern=".{8,30}" required />
	</p>
	<input type="submit" value="Envoyer" />
</form>
<?php $this->includeView("footer"); ?>
