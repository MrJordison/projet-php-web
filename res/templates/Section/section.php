<?php $this->defineVar("header", "title", $datasec["titre"]."- Forum");
$this->includeView("header"); 
if ($datasec["idParent"]!=null) {?>
<a class="retour" href="<?php echo dirname($_SERVER['PHP_SELF']).'/section/'.$datasec["idParent"]; ?>"><p class="retour">⇦ Retour</p></a>
<?php } ?>
<h2><?php echo $datasec["titre"]; ?></h2>
<p class="description">
	<?php echo $datasec["description"]; ?>
</p>
<?php if (count($datasec["sections"]) > 0) {?>
<div class="listesection">
	<div class="listesujetsection etiquette">
		<p class="titresection">Section</p>
		<p class="nbsujets">Sujets</p>
		<p class="nbmessages">Messages</p>
	</div>
	<?php foreach ($datasec["sections"] as $sec) {?>
	<a href="<?php echo dirname($_SERVER['PHP_SELF']).'/section/'.$sec["id"]; ?>" class="aclean"><div class="listesujetsection">
		<p class="titresection"><?php echo $sec["titre"]; ?></p>
		<p class="nbsujets"><?php echo $sec["nbSujet"]; ?></p>
		<p class="nbmessages"><?php echo $sec["nbMessage"]; ?></p>
	</div></a>
	<?php } 
} else {
$this->defineVar("boutonsujet", "action", $datasec["id"]);
$this->includeView("boutonsujet");?>
<div class="listesujet">
	<div class="listesujetsection etiquette">
		<p class="titresujet">Sujet</p>
		<p class="nbreponses">Réponses</p>
		<p class="lederniermessage">Dernier message</p>
	</div>
	<?php foreach ($datasec["sujets"] as $suj) {?>
	<a href="<?php echo dirname($_SERVER['PHP_SELF']).'/sujet/'.$suj["id"]; ?>" class="aclean"><div class="listesujetsection">
		<p class="titresujet"><?php echo $suj["titre"]; ?></p>
		<p class="nbreponses"><?php echo $suj["nbMessage"]; ?></p>
		<p class="lederniermessage"><?php echo $suj["answer"]; ?><br /> <?php echo $suj["date"]; ?></p>
	</div></a>
	<?php }
$this->includeView("boutonsujet"); 
}?>
</div>
<?php $this->includeView("footer"); ?>
