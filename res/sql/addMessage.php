<?php
	function addMessage($connexion, $t, $iduser, $idsujet)
	{
		$request= "call addMessage(:texte,:user,:sujet)";
		$stmt = $connexion->prepStatement("call addMessage(:texte,:user,:sujet)");
		$stmt->bindParam(':texte',$t);
		$stmt->bindParam(':user',$iduser);
		$stmt->bindParam(':sujet',$idsujet);
		$stmt->execute();	
	}

