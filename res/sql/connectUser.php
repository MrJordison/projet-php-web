<?php

require 'src/instances/User.php';

function connectUser($conn, $login, $pwd) {
    $req = "SELECT id, pseudo, mail, description, niveauDroit, mdp FROM User WHERE pseudo=:login";
    $stmt = $conn->prepStatement($req);

    $stmt->bindParam(":login", $login);

    $stmt->execute();

    if ($v = $stmt->fetch()) {
        if (password_verify($pwd, $v['mdp']))
            return new User($v['id'], $v['pseudo'], $v['mail'], $v['description'], $v['niveauDroit']);
        else
            return NULL;
    }
    else {
        return NULL;
    }
}

    
