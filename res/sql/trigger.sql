
DROP TRIGGER IF EXISTS incrNbMessage;
DROP TRIGGER IF EXISTS decrNbMessage;
DROP TRIGGER IF EXISTS incrNbSujet;
DROP TRIGGER IF EXISTS decrNbSujet;

CREATE TRIGGER incrNbSujet
	AFTER INSERT
	ON Sujet
	FOR EACH ROW
	BEGIN
		CALL majNbSujetSection(NEW.idSection,1);
END;

CREATE TRIGGER decrNbSujet
	AFTER DELETE
	ON Sujet
	FOR EACH ROW
	BEGIN
		CALL majNbSujetSection(OLD.idSection,-1);
END;


CREATE TRIGGER incrNbMessage
    AFTER INSERT
    ON Message
    FOR EACH ROW
    BEGIN
	DECLARE section INTEGER(5);
	UPDATE Sujet
                SET nbMessage =  nbMessage +1
                WHERE id = NEW.idParent;
	SELECT idSection INTO section FROM Sujet
			WHERE id=NEW.idParent;
	CALL majNbMessageSection(section,1);

END;

CREATE TRIGGER decrNbMessage
    AFTER DELETE
    ON Message
    FOR EACH ROW
    BEGIN
	DECLARE section INTEGER(5);
        UPDATE Sujet
        	SET nbMessage = nbMessage - 1
            WHERE id = OLD.idParent;
	SELECT idSection INTO section FROM Sujet
		WHERE id=OLD.idParent;
	CALL majNbMessageSection(section,-1);
END;

