<?php
        //require (dirname(__FILE__)."/comptSujet.php");
	//require (dirname(__FILE__)."/comptMessage.php");
	error_reporting(E_ALL);
	function affSection($connexion,$idVoulu)
	{
		$dataSection = array();
		$result = $connexion->resultQuery("select * from Section where id=".$idVoulu);
		if($result!=null)
		{
			foreach($result as $r){
			$dataSection['titre']=$r['titre'];
			$dataSection['id']=$idVoulu;
			$dataSection['description']=$r['description'];
			$dataSection['idParent']=$r['idParent'];
			$dataSection['nbSujet']=$r['nbSujet'];
			$dataSection['nbMessage']=$r['nbMessage'];
			}
			$result = $connexion->resultQuery("select * from Section where idParent=".$idVoulu);
			$dataSection['sections']=array();
			if ($result!=null)
			{
				foreach($result as $m)
				{
					array_push($dataSection['sections'],
						array(
							"id"=>$m['id'],
							"titre"=>$m['titre'],
							"description"=>$m['description'],
							"nbSujet"=>$m['nbSujet'],
							"nbMessage"=>$m['nbMessage'],
						)
					);
				}
				$dataSection['sujets']=array();
				$result = $connexion->resultQuery("select s.id, s.titre,
					DATE_FORMAT(s.date, '%Hh%i le %d/%m/%Y') as dateCreation,
					 u.pseudo, s.nbMessage, u2.pseudo as answer,
					  DATE_FORMAT(m.date, '%Hh%i le %d/%m/%Y') as date
					   from Sujet s, User u, User u2,
					   Message m where s.idSection = ".$idVoulu." 
					   and s.auteur = u.id and m.idParent=s.id 
					   and u2.id=m.auteur order by date desc");
				if ($result!=null)
				{
					$test=array();
					foreach($result as $m)
					{
						if(!in_array($m['id'],$test))
						{
							array_push($test,$m['id']);
							array_push($dataSection['sujets'],
								array(
									"id"=>$m['id'],
									"titre"=>$m['titre'],
									"dateCreation"=>$m['dateCreation'],
									"auteur"=>$m['pseudo'],
									"nbMessage"=>$m['nbMessage'],
									"answer"=>$m['answer'],
									"date"=>$m['date']
								)
							);
						}
					}
				}
			}
		}
	return $dataSection;
	}
?>
