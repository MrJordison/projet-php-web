DROP PROCEDURE IF EXISTS addUser;
DROP PROCEDURE IF EXISTS addSection;
DROP PROCEDURE IF EXISTS addSujet;
DROP PROCEDURE IF EXISTS addMessage;
DROP PROCEDURE IF EXISTS createSujet;
DROP PROCEDURE IF EXISTS majNbMessageSection;
DROP PROCEDURE IF EXISTS majNbSujetSection;

CREATE PROCEDURE majNbSujetSection
	(
		IN section	INTEGER(5),
		IN i		INTEGER(1)
	)
	BEGIN
		DECLARE parent INTEGER(5) DEFAULT -1;
		SET @@SESSION.max_sp_recursion_depth=255;
		UPDATE Section
			SET nbSujet = nbSujet + i
			WHERE id = section;
		SELECT idParent INTO parent
			FROM Section
			WHERE id=section;
		IF parent!=-1 THEN
			CALL majNbSujetSection(parent,i);
		END IF;
END;

CREATE PROCEDURE majNbMessageSection
	(
		IN section	integer(5),
		IN i		integer(1)
	)
	BEGIN	
		DECLARE parent INTEGER(5) DEFAULT -1;
		SET @@SESSION.max_sp_recursion_depth=255;
		UPDATE Section 
			SET nbMessage= nbMessage + i
			WHERE id = section;
		SELECT idParent INTO parent
			FROM Section 
			WHERE id=section;
		IF parent!=-1 THEN
			CALL majNbMessageSection(parent,i);
		END IF;
END;
 


CREATE PROCEDURE addUser
    (
        IN p       varchar(20), 
        IN psw     BINARY(60),
        IN addr    varchar(40),
        IN descr   text
    )
    BEGIN 
        INSERT INTO User  
            (
                pseudo,
                mdp,
                mail, 
                description, 
                niveauDroit
            )
        VALUES 
            (
                p,
                psw,
                addr,
                descr,
                1
            );
END;


CREATE PROCEDURE addSection
    (
        IN parent   int(5),
        IN t        varchar(50),
        IN descr    text
    )
    BEGIN
        INSERT INTO Section 
        (
            titre, 
            description, 
            idParent,
		nbMessage,
		nbSujet
        ) 
        VALUES 
            (
                t, 
                descr, 
                parent,
		0,
		0
            );
END;

CREATE PROCEDURE addSujet
    (
        IN t varchar(50),
        IN idsection int(5),
        IN iduser int(5)
    )
    BEGIN
        INSERT INTO Sujet 
            (
                titre, 
                etat, 
                date,
                idSection,
                auteur,
                nbMessage
            ) 
        VALUES 
            (
                t,
                2,
                NOW(),
                idsection,
                iduser,
                0
            );
END;

CREATE PROCEDURE addMessage
    (
        m text, 
        iduser int(5), 
        idsujet int(5)
    )
    BEGIN
        INSERT INTO Message 
            (
                corps,
                date,
                auteur,
                idParent,
                visible
            ) 
        VALUES 
            (
                m,
                NOW(),
                iduser,
                idsujet,
                1
            );
END;


CREATE PROCEDURE createSujet
    (
        IN t varchar(50),
        IN idsection int(5),
        IN iduser int(5),
        IN m text
    )
    BEGIN
        DECLARE idSujet int DEFAULT 0;
        CALL addSujet
            (
                t,
                idsection,
                iduser
            );
        CALL addMessage
            (
                m,
                iduser,
                LAST_INSERT_ID()
            );
END;

