drop table if exists Message;
drop table if exists Sujet;
drop table if exists Section;
drop table if exists User;

create table User(
	id int(5) primary key auto_increment,
	pseudo varchar(20) not null,
	mdp binary(60) not null,
	mail varchar(40) not null,
	description text,
	niveauDroit int(1) not null
);

create table Section(
	id int(5) primary key auto_increment,
	titre varchar(50) not null,
	description text,
	idParent int(5),
	nbMessage integer(10),
	nbSujet integer(10)
);

create table Sujet(
	id int(5) primary key auto_increment,
	titre varchar(50) not null,
	etat int(1) not null,
	date DATETIME not null,
	idSection int(5) not null,
	auteur int(5) not null,
	nbMessage int(5) not null,
	constraint foreign key FKidsection (idSection) references Section(id),
	constraint foreign key (auteur) references User(id)
);

create table Message(
	id int(5) primary key auto_increment,
	corps Text not null,
	date DATETIME not null,
	auteur int(5) not null,
	idParent int(5) not null,
	visible int(1) not null,
	constraint foreign key (auteur) references User(id),
	constraint foreign key (idParent) references Sujet(id)
);
