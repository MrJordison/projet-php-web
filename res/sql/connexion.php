<?php
	date_default_timezone_get("Europe/Paris");
	class DBConnexion{
		private static $connexion=null;
		function __construct(){
			if(self::$connexion==null){
                $config = include('config.php');
                self::$connexion = new PDO('mysql:host='.$config['mysqlHost'].';dbname='.$config['mysqlBdd'],$config['mysqlUser'],$config['mysqlPwd'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
				self::$connexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
		}

		function prepStatement($requete){
			return self::$connexion->prepare($requete);
		}
		
		function resultQuery($requete){
			return self::$connexion->query($requete);
		}

                function exec($req) {
                    self::$connexion->exec($req);
                }
        function lastInsertId(){
			return self::$connexion->lastInsertId();
		}
	}
