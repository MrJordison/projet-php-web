<?php
	function createSujet($connexion, $t, $text,$idSect, $idAuteur)
	{
		$stmt = $connexion->prepStatement("call createSujet(:titre,:section,:auteur,:message)");
		$stmt->bindParam(':titre',$t);
		$stmt->bindParam(':section',$idSect);
		$stmt->bindParam(':auteur',$idAuteur);
		$stmt->bindParam(':message',$text);
		$stmt->execute();
		$stmt = $connexion->prepStatement("SELECT idParent as lastId FROM Message where id=LAST_INSERT_ID()");
		$stmt->execute();
		$lastd=$stmt->fetch();
		return $lastd;
	}
