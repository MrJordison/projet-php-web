<?php
	function affSujet($connexion, $idVoulu)
	{
		$dataSujet = array();
		$result = $connexion->resultQuery("select * from Sujet where id =".$idVoulu);
		if($result!=null)
		{
			foreach($result as $m)
			{
			$dataSujet['titre']=$m['titre'];
			$dataSujet['id']=$idVoulu;
			$dataSujet['etat']=$m['etat'];
			$dataSujet['idSection']=$m['idSection'];
			$dataSujet['date']=$m['date'];
			}
			$dataSujet['messages']=array();
			$result = $connexion->resultQuery("select u.pseudo, m.corps, DATE_FORMAT(m.date, '%d/%m/%Y à %Hh%imin%ss') as date_rep from Message m, User u where u.id=m.auteur and m.idParent=".$idVoulu." order by date_rep");	
			if ($result!=null)
			{
				foreach($result as $m)
				{
					array_push($dataSujet['messages'],
						array(
							"auteur"=>$m['pseudo'],
							"corps"=>$m['corps'],
							"date"=>$m['date_rep']
						)
					);
				}
			}
		}
		return $dataSujet;
	}
?>
