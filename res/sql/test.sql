insert into Section (titre, description,nbMessage,nbSujet)
	values ('Accueil', 'Bienvenue sur notre forum !',0,0);
insert into Section (titre, description, idParent,nbMessage,nbSujet)
	values ('Sous forum', 'Voici un sous-forum qui ne contient que des sujets', 1,0,0);
insert into Section (titre, description, idParent,nbMessage,nbSujet)
	values ('Sous Section', 'Voici une sous section qui ne contient que des sections', 1,0,0);
insert into Section (titre, description, idParent,nbMessage,nbSujet)
	values ('Sous-Sous Section', 'Voici une sous-sous section qui ne coûte pas très chère....', 3,0,0);
insert into Section (titre, description, idParent,nbMessage,nbSujet)
	values ('Un dernière Section', 'Voici la dernière section de ce jeu de test', 3,0,0);

insert into User (pseudo, mdp,mail, description, niveauDroit)
	values ('ping', '$2y$10$WfTMgQ2Ffmr6AeK9AGNA0eMc4ENSbd.0.12619oDceGsHwiJAgr8C', 'pingping@hostname.fr','Bonjour je suis ping',1);
insert into User (pseudo, mdp,mail, description, niveauDroit)
	values ('pong', '$2y$10$O3lpiuZrxgbYoI5yll3A8eZMXHv/.vngOljwWVUn65qcwnAqA1oxK', 'pong@host.com','Appellez moi pong',1);

call createSujet('Bienvenu', 2, 1, 'Bonjour et bienvenu sur notre forum.\r\nJ\'espère que vous y passerez un agréable moment.\r\n\r\nCordialement,\r\nVotre chef pong !');
call addMessage('Ahah vous avez vu la petite blague ?\r\nEn fait moi c\'est ping !',1,1);
call createSujet('Topikiserarion', 2, 1, 'Comme son nom l\'indique.');
call addMessage('Ba oui ! c\'est moi pong !!\r\nT\'es trop drôle.',2,1);
call addMessage('POSTKISERARIEN\r\n\r\nt\'as fait une faute dans le titre hihi',2,2);
call addMessage('LOL',1,1);
call createSujet('Vos bon plan', 4, 1, 'Salut,\r\nOui ici on est tous (ou presque) étudiant et dès qu\'il s\'agit d\'économiser le moindre centime tout le monde est intéressé.\r\nPartagez donc ici vos meilleurs bon plan !');
call addMessage('En adhérant à Infasso on a le droit à -20% à la fête et jusqu\'à -60% chez Dell !!',2,3);
call createSujet('Cours de latin', 4, 1, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed feugiat at justo eget consectetur. Nunc aliquam ut tortor eget aliquet. Aliquam consectetur erat in vestibulum mattis. Etiam consequat velit quis augue varius malesuada. Proin a placerat lacus, ut egestas massa. Aenean at lacus non nibh hendrerit eleifend nec nec felis. Duis placerat tortor tortor, vel tincidunt magna porttitor in. Suspendisse fermentum auctor lorem eget faucibus. Donec dictum tellus metus, ac gravida orci sollicitudin tincidunt. Phasellus lobortis nibh id molestie vulputate. Donec id commodo orci. Nullam eu sem non massa imperdiet imperdiet. Vestibulum id egestas enim, a tincidunt mauris. Etiam ultricies mauris eget nulla feugiat ultricies.\r\n\r\nUt vulputate varius augue aliquet mollis. Fusce ac fermentum enim, sed scelerisque lectus. Aenean accumsan aliquet facilisis. Curabitur quis condimentum sem. Nunc nulla augue, blandit at pellentesque id, molestie ut ex. Quisque in suscipit orci. Vivamus non ligula massa.');
call addMessage('ah pas mal !',1,3);
call createSujet('Le dernier sujet avant la fin du monde', 5, 1, 'Adieu');
call addMessage('Soit pas pessimiste comme ça ping.',2,5);
call addMessage('Interdum et malesuada fames ac ante ipsum primis in faucibus. Quisque lectus elit, pulvinar id nulla et, tincidunt eleifend nunc. Proin eu ultricies arcu. Nam euismod luctus nisl, a hendrerit enim convallis vitae. Sed venenatis enim enim, ut semper purus molestie a. In id feugiat augue. Aliquam tempus, mauris non ornare hendrerit, ipsum mi aliquam ipsum, non facilisis erat dolor vel urna. Etiam ut aliquam ipsum, vitae faucibus augue. Aliquam tempor, arcu eu tempus dignissim, tellus lorem elementum lacus, eu posuere diam dui at ante. Ut non pretium lectus. Vivamus sagittis lacus vitae massa semper, id feugiat quam posuere. Nunc volutpat tellus facilisis rhoncus tincidunt. Integer mi lorem, tempor in velit eu, interdum mollis est.',2,4);
call addMessage(':\'(',1,5);
