<?php

require 'res/sql/connectUser.php';

class UserController extends ControllerBase {

    function __construct() {
        parent::__construct();
    }

    public function connectAction() {
        $login = $_POST['login'];
        $pwd = $_POST['pwd'];

        $conn = new DBConnexion();

        $userConnected = connectUser($conn, $login, $pwd);

        if ($userConnected == NULL) {
            $this->setView("err","base");
			$this->render(array(
			'err' => "Erreur :<br />Le couple login/mot de passe n'est pas correct.",));
            return;
        }

        $this->setUser($userConnected);
        $this->saveUserSession();

        $this->sendInfo("Vous êtes maintenant connecté !");
        $this->redirect($_SERVER['HTTP_REFERER']);

    }

    public function disconnectAction() {
        if ($this->user != NULL) {
            $this->rmUserSession();
            $this->sendInfo("Vous êtes maintenant déconnecté !");
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
        else {
            $this->sendInfo("Vous devez être connecté pour vous déconnecter");
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
    }
}


