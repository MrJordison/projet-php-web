<?php
	require (dirname(__FILE__)).'/../../res/sql/addMessage.php';
	require (dirname(__FILE__)).'/../../res/sql/createSujet.php';
	class FormulaireController extends ControllerBase
	{
		function __construct()
		{
			parent::__construct();
		}
		
		public function repondreAction($id=null)
		{	
            $this->lvlNeeded(1);
			if ($id==null){
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur 404 :<br />Le sujet demandé n'existe pas, impossible d'y répondre.",));
			} else {
				$sql = new DBConnexion();
				$res = affSujet($sql,$id);
				if (count($res) <= 1) {
					$this->setView("err","base");
					$this->render(array(
					'err' => "Erreur 404 :<br />Le sujet demandé n'existe pas, impossible d'y répondre.",));
				} else {
					$this->setView("repondre","Formulaire");
					$this->render(array(
		            'datasuj' => $res,
						));
				}
			}
		}
		
		public function sujetAction($id=null){
			if ($id==null){
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur 404 :<br />La section demandée n'existe pas, impossible d'y ajouter un sujet.",));
			} else {
				$this->setView("formsujet","Formulaire");
					$this->render(array(
					'id' => $id));
			}
		}
		
		public function inscriptionAction(){
			$this->setView("inscription","Formulaire");
			$this->render(array());
		}
		
		public function inscrireAction(){
			if (isset($_POST["login"],$_POST["mail"],$_POST["password"])) {
				$pwd = password_hash($_POST["password"], PASSWORD_DEFAULT);
				$sql = new DBConnexion();
				$stmt = $sql->prepStatement("call addUser(:login,:pwd,:mail,:desc)");
				$stmt->bindParam(':login',$_POST["login"]);
				$stmt->bindParam(':pwd',$pwd);
				$stmt->bindParam(':mail',$_POST["mail"]);
				$stmt->bindParam(':desc',$_POST["description"]);
				$stmt->execute();	
				$this->setView("inscrire","Formulaire");
				$this->render(array(
					"login" => $_POST["login"]));
			} else {
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur :<br />Bien essayé mais on ne s'inscrit pas de cette façon là !",));
			}
		}
		
		public function nouveausujetAction($id=null){
            $this->lvlNeeded(1);
			if (isset($_POST["message"],$_POST["titre"])){
				$sql = new DBConnexion();
				$newid = createSujet($sql,$_POST["titre"],$_POST["message"],$id,$this->user->getId());
				header('Location: '.$_SERVER['PHP_SELF'].'/../sujet/'.$newid[0]);
				exit();
				
			} else {
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur :<br />Bien essayé mais on ne créé pas de sujet de cette façon là !",));
			}
		}
		
		public function newmessageAction($id=null){
            $this->lvlNeeded(1);
			if (isset($_POST["message"])){
				$sql = new DBConnexion();
				addMessage($sql, $_POST["message"], $this->user->getId(), $id);
				header('Location: '.$_SERVER['PHP_SELF'].'/../sujet/'.$id);
				exit();
			} else {
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur :<br />Bien essayé mais on ne répond pas à un sujet de cette façon là !",));
			}
		}
	}

?>
