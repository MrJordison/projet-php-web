<?php

	require (dirname(__FILE__)).'/../../res/sql/affSection.php';
	
	class SectionController extends ControllerBase
	{
		function __construct()
		{
			parent::__construct();
		}
		
		public function sectionAction($id=1)
		{
			$sql = new DBConnexion();
			$res=affSection($sql,$id);
			if (count($res) <= 2) {
				$this->setView("err","base");
				$this->render(array(
				'err' => "Erreur 404 :<br />La section demandée n'existe pas.",));
			} else {
				$this->render(array(
	            'datasec' => $res,
			));
			}
		}
	}

?>
