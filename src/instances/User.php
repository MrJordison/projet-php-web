<?php

class User {

    protected $id;
    protected $pseudo;
    protected $mail;
    protected $description;
    protected $niveauDroit;

    function __construct($id, $pseudo, $mail, $desc, $lvl) {
        $this->id = $id;
        $this->pseudo = $pseudo;
        $this->mail = $mail;
        $this->description = $desc;
        $this->niveauDroit = $lvl;
    }

    function getId() { return $this->id; }
    function getPseudo() { return $this->pseudo; }
    function getMail() { return $this->mail; }
    function getDesc() { return $this->description; }
    function getLvl() { return $this->niveauDroit; }

}
