<?php

require (dirname(__FILE__)).'/base/ControllerBase.php';
require (dirname(__FILE__)).'/../res/sql/connexion.php';
require 'HelloWorld/HelloWorldController.php';
require 'Section/SectionController.php';
require 'Sujet/SujetController.php';
require 'Formulaire/FormulaireController.php';
require 'User/UserController.php';

/**
 * Parse l'adresse et exécute le bon
 * module
 */
class Router {

    protected $routes = array();
    protected $err404;


    /**
     * Test toutes les règles et lance le bon controlleur
     */
    public function run() {


        $basepath = "projet-php-web/";
        $URLroute = urldecode(substr($_SERVER["REQUEST_URI"], strpos($_SERVER["REQUEST_URI"], $basepath)+strlen($basepath))); 

        $explURL = explode("/", $URLroute);
        $routeFinal = null;

        foreach ($this->routes as $route) {
            $explRoute = explode("/", $route['pattern']);
            $found=True;
            $params = array();
            for ($i=0; $i<count($explURL); $i++) {
                if ($explURL[$i] != "") {
                    if (preg_match("/^\{(.*)\}$/", $explRoute[$i], $match))
                        $params[$match[0]] = $explURL[$i];
                    else if ($explRoute[$i] != $explURL[$i])
                        $found = False;
                }
            }
            if ($found) {
                $this->runRoute($route, $params);
                return;
            }
        }

        $this->send404();
    }

    /**
     * Ajouter une règle au router
     * $name Str Nom de la règle
     * $pattern Str Règle d'execution de la fonction
     * $funct Closure Fonction de callback, appellée si la règle est vérifiée
     */
    public function addRoute($name, $pattern, $controller, $action) {
        $this->routes[$name] = array (
            'pattern' => $pattern,
            'contr' => $controller,
            'action' => $action
        );
    }


    /**
     * Définie la fonction à appeller lors d'une erreur 404
     * Closure $funct Fonction de callback
     */
    public function set404($funct) {
        $this->err404 = $funct;
    }

    private function send404() {
        header("HTTP/1.0 404 Not Found");
        if ($this->err404 != null)
            $this->err404->__invoke(); //un petit peu tricky, appeller $this->err404() ne fonctionne pas
        else
            echo "Woops, la page demandee est inexistante";
    }

    private function runRoute($route, $params) {
        $controllerName = $route['contr']."Controller";
        $controller = new $controllerName();

        $action = $route['action']."Action";

        call_user_func_array(array($controller, $action), $params);
    }
    


}
