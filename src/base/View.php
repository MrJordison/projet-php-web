<?php


/**
 * La vue du modèle MVC
 */
class View {

    protected $path;
    protected $datas;
    protected $currentName;

    function __construct($controller, $action) {
        $this->path = $this->getTemplatePath($controller, $action);
    }


    /**
     * Génère un chemin vers un template
     * @param string $controller
     * @param string $action
     */
    protected function getTemplatePath($controller, $action) {
        return dirname(__file__)."/../../res/templates/".$controller."/".$action.".php";
    }

    /**
     * Définir les données à utiliser dans le template
     * @param array $d Données
     */
    public function setDatas($d) {
        $this->datas['this'] = $d;
    }

    /**
     * Envoyer le template parsé au navigateur client
     */
    public function render() {
        try{
            if (!file_exists($this->path)) {
                throw new Exception("Error : Vue inexistante (".$this->path.")");
            }

            extract($this->datas['this']);
            ob_start(); //on met tout dans le buffer
            include($this->path);
            $content = ob_get_contents();
            ob_end_flush(); //libération du buffer
            return $content;
        }
        catch (Exception $e) {echo $e->getMessage();}
    }

    /**
     * Définie une variable à inclure dans le template
     * @param string $templateName Nom du template dans lequel inclure les variables
     * @param string $varName Nom de la variable
     * @param $value Valeur à donner à la variable
     */
    protected function defineVar($templateName, $varName, $value) {
        $this->datas[$templateName][$varName] = $value;
    }

    /**
     * Inclure le template dans le template actuel et y ajouter les variables définies
     * @param string $name Nom de la template du dossier base à ajouter
     */
    protected function includeView($name) {
        $this->currentName = $name;
        extract($this->datas['this']);
        include($this->getTemplatePath("base", $name));
    }

    protected function getVar($varName, $defaultValue) {
        if (isset($this->datas[$this->currentName][$varName]))
            return $this->datas[$this->currentName][$varName];
        else
            return $defaultValue;
    }

    protected function includeVar($varName, $defaultValue) {
        return $this->getVar($varName, $defaultValue);
    }

}
