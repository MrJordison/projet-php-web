<?php

require 'View.php';

/**
 * Controller de base, parent des controlleurs de l'application
 * Pour créer une action à l'intérieur du controlleur créer une nouvelle méthode appellée trucAction (toujours mettre "Action" en suffixe). Lister les paramètres de votre action, ceux-ci seront automatiquement définis.
 * @exemple public function newAction($id = 0, $name = "")
 */
class ControllerBase {

    protected $view;
    protected $controllerName;
    protected $user; 

    function __construct() {
        if (!session_start())
            throw new Exception("Impossible de démarrer une session");
        $this->controllerName = substr(get_class($this), 0, -10);
        $this->getUserSession();
    }

    /**
     * Crée un rendu de la vue par défaut si view n'a pas été définie
     * @param array $datas Données à passer à la vue
     */
    public function render($datas = array()) {
        if (!isset($this->view)) {
            $action = substr(debug_backtrace()[1]['function'], 0, -6); //un peu sale, mais ça fonctionne
            $this->view = new View($this->controllerName, $action);
        }

        $datas['User'] = $this->user;
        if (isset($_SESSION['infos'])) {
            $datas['infos'] = $_SESSION['infos'];
            $this->rmInfos(); //si les infos ont été vu, pas besoin de les voir à nouveau
        }

        $this->view->setDatas($datas);
        $this->view->render();
    }

    /**
     * Redirige l'utilisateur vers une autre page
     * @param string url Lien vers la page
     */
    public function redirect($url) {
        header("Location: ".$url);
        exit;
    }


    /**
     * Définie le couple controller/action à rendre
     * @param string $controller le nom du controller (nom du dossier)
     * @param string $action le nom de la vue (nom du fichier)
     */
    public function setView($action, $controller = NULL) {
        if (is_null($controller))
            $controller = $this->controllerName; //pas le choix, on ne peut pas utiliser $this dans les arguments
            
        $this->view = new View($controller, $action);
    }

    public function setUser($user) {
        $this->user = $user;
    }

    public function saveUserSession() {
        $_SESSION['user'] = serialize($this->user);
    }

    public function getUserSession() {
        if (isset($_SESSION['user'])) {
            $this->user = unserialize($_SESSION['user']);
        }
    }

    public function rmUserSession() {
        $_SESSION['user'] = NULL;
    }

    /**
     * Ajoute une info à afficher au prochain rendu
     * @param string Info à afficher
     */
    public function sendInfo($info) {
        $_SESSION['infos'][] = $info;
    }

    /**
     * Supprime toutes les infos de la session
     */
    public function rmInfos() {
        $_SESSION['infos'] = NULL;
    }

    /**
     * Vérifie si l'utilisateur a les droits d'accès suffisant
     * @param integer $lvl indique le niveau minimum pour accéder à l'action, -1 pour accès anonyme
     */
    public function lvlNeeded($lvl) {
        if ($lvl == -1)
            return true;
        else {
            if ($this->user != NULL) {
                if ($this->user->getLvl() >= $lvl)
                    return true;
                else {
                    $this->sendInfo("Vous n'avez pas le droit d'accéder à cette page");
                    $this->redirect(dirname($_SERVER['PHP_SELF']));
                }
            }
            else {
                $this->sendInfo("Vous devez être connecté pour accéder à cette page");
                $this->redirect(dirname($_SERVER['PHP_SELF']));
            }
        }
    }
}
