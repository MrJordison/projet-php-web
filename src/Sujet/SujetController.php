<?php
	require (dirname(__FILE__)).'/../../res/sql/affSujet.php';
	
	class SujetController extends ControllerBase
	{
		function __construct()
		{
			parent::__construct();
		}
		
		public function sujetAction($id=null)
		{
			if ($id==null) {
				$this->setView("err","base");
				$this->render(array(
					'err' => "Le sujet demandé n'existe pas",));
			} else {
				$sql = new DBConnexion();
				$res = affSujet($sql,$id);
				if (count($res) <= 1) {
					$this->setView("err","base");
					$this->render(array(
					'err' => "Erreur 404 :<br />Le sujet demandé n'existe pas.",));
				} else {
				$this->render(array(
	            'datasuj' => $res,
				));
				}
			}
		}
	}

?>

