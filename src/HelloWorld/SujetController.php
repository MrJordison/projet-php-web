<?php
	require (dirname(__FILE__)).'/../base/ControllerBase.php';
	require (dirname(__FILE__)).'/../../res/sql/affSujet.php';
	
	class SujetController extends ControllerBase
	{
		function __construct()
		{
			parent::__construct();
		}
		
		public function affSujetAction($connexion, $idVoulu)
		{
			$this->render(affSujet($connexion, $idVoulu));
		}
	}
