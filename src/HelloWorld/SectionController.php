<?php
	require (dirname(__FILE__)).'/../base/ControllerBase.php';
	require (dirname(__FILE__)).'/../../res/sql/affSection.php';
	
	class SectionController extends ControllerBase
	{
		function __construct()
		{
			parent::__construct();
		}
		
		public function affSectionAction($connexion, $idVoulu)
		{
			$this->render(affSection($connexion, $idVoulu));
		}
	}

