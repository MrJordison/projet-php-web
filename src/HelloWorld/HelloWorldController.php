<?php

class HelloWorldController extends ControllerBase{

    function __construct() {
        parent::__construct();
    }

    public function helloAction($name = "world") {
        $this->render(array(
            'name' => $name,
        ));
    }
}
