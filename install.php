<?php
require "res/sql/connexion.php";

echo "Installation de la base de donnees en cours.... (prenez un cafe, on s'occupe de tout !)<br/>";

$filesName = array("creationdb", "proc", "trigger", "test");
$conn = new DBConnexion();

foreach ($filesName as $fileName) {
    $reqs = "";
    echo "Lecture du fichier ".$fileName.".sql<br/>";
    if ($fileName == "creationdb" && $fileName == "test") {
        $file = file("res/sql/".$fileName.".sql");
        foreach ($file as $l) {
            if (substr(trim($l), 0, 2) != "--") //suppression des commentaires
                $reqs .= $l;
        }

        $reqs = explode(";", $reqs);
        foreach($reqs as $req) {
            if ($req != "\n") {
                $conn->exec($req);
            }
        }
    }
    else {
        $file = file_get_contents("res/sql/".$fileName.".sql");
        $conn->exec($file);
    }
}

echo "Installation realisee avec succes ! ";
echo "Vous pouvez maintenant lancer l'application : <br/>";
echo "<a href=\".\"> C'est parti ! </a>";

